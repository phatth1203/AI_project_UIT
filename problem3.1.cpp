#include<iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;
 
int main() {
    int v, e, n;
    cin >> v >> e;
    map<string, int> mymap;
    string *data = new string[v];
    for (int i = 0; i < v; i++) {
        cin >> data[i];
        mymap[data[i]] = i;
    }
    vector<vector<bool>> graph(v, vector<bool>(v));
    for (int i = 0; i < e; i++) {
        string u, j;
        cin >> u >> j;
        graph[mymap[u]][mymap[j]] = 1;
    }
    for (int i = 0; i < v; i++) {
        int count = 0;
        for (int j = 0; j < v; j++) {
            if (graph[i][j] == 1 || graph[j][i] == 1)
                count++;
        }
        cout << count << " ";
    }
    cout << endl;
    system("pause");
    return 0;
}