#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
 
struct exec {
    int time, mch;
};
 
bool check(vector<int> database, int x) {
    for (int i = 0; i < database.size(); i++) {
        if (database[i] == x) return true;
    }
    return false;
}
 
int MIN(vector<int> machine, int m) {
    int min = 1000000, imin = 0;
    for (int i = 0; i < m; i++) {
        if (machine[i] < min) {
            min = machine[i];
            imin = i;
        }
    }
    return imin;
}
 
int main() {
    int n, m;
    cin >> n >> m;
    vector<exec> ex;
    vector<int> machine;
    vector<int> data;
    for (int i = 0; i < m; i++)
        machine.push_back(0);
    for (int i = 0; i < n; i++) {
        exec t;
        cin >> t.time;
        t.mch = 0;
        ex.push_back(t);
    }
    for (int i = 0; i < n; i++) {
        int max = 0;
        int imax = 0;
        for(int j=0;j<n;j++){
            if (ex[j].time > max) {
                max = ex[j].time;
                imax = j;
            }
        }
    //    cout << max << "max ";
        int min = MIN(machine, m);
//        cout << min << "min ";
        machine[min] += max;
        ex[imax].time = 0;
        ex[imax].mch = min;
    }
    cout << endl;
    for (int i = 0; i < n; i++)
        cout << ex[i].mch << " ";
    system("pause");    
    return 0;
}