#include <iostream>
#include <vector>
#include <algorithm>
#define infi = 999
using namespace std;
 
int iMin(vector<long> b) {
    int min = b[0];
    int imin = 0;
    for (int i = 0; i < b.size(); i++) {
        if (b[i] <= min) {
            min = b[i];
            imin = i;
        }
    }
    return imin;
}
int main() {
    int n, m;
    cin >> n >> m;
    vector<long> machine;
    for (int i = 0; i < m; i++) {
        machine.push_back(0);
    }
    int **col = new int*[m];
    for (int i = 0; i < m; i++)
        col[i] = new int[n];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            cin >> col[i][j];
        }
    }
    for (int i = 0; i < n; i++) {
        int imin = 0; 
        for (int j = 0; j < m; j++) {
            if (col[j][i] < 0)
                machine[j] += 9999999999;
            machine[j] += col[j][i];
        }
        imin = iMin(machine);
        for (int z = 0; z < m; z++) {
            if (z != imin) {
                if (col[z][i] < 0)
                    machine[z] -= 9999999999;
                machine[z] -= col[z][i];
            }
        }
        for (int u = 0; u < m; u++) {
        //    cout << machine[u] << " ";
        }
        //cout << endl;
        cout << imin << " ";
    }
    system("pause");
    return 0;
}