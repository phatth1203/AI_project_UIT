#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
 
bool check(vector<int> database, int x) {
    for (int i = 0; i < database.size(); i++) {
        if (database[i] == x) return true;
    }
    return false;
}
 
int main() {
    int n, m;
    cin >> n >> m;
    vector<int> time;
    vector<int> database;
    vector<int> machine;
    for (int i = 0; i < m; i++) {
        machine.push_back(0);
    }
    for (int i = 0; i < n; i++) {
        int t; cin >> t;
        database.push_back(i);
        time.push_back(t);
    }
    int min=10000, imin=10000;
    for (int i = 0; i < m; i++) {
        int num;
        do {
            cin >> num;
            if (num != -1) {
                database[num] = 1000000;
                machine[i] += time[num];
            }
        } while (num != -1);
        if (machine[i] < min) {
            min = machine[i];
            imin = i;
        }
    }
    
 
    for (int i = 0; i < database.size(); i++) {
        if(database[i]<=n)
            cout << database[i] << " ";
    }
    cout << endl;
    cout << imin << " " << machine[imin];
    system("pause");    
    return 0;
}