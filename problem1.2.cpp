#include<iostream>
#include <vector>
using namespace std;
 
 
struct handling
{
    int num, u, i;
};
void nhap(handling &a) {
    cin >> a.num;
    if (a.num == 1) {
        cin >> a.u >> a.i;
        a.u -= 1;
        a.i -= 1;
    }
    else {
        cin >> a.u;
        a.u -= 1;
    }
}
void output(vector<vector<bool>> graph, int v, int n, handling *a) {
    bool check;
    for (int i = 0; i < n; i++) {
        if (a[i].num == 1) {
            if (graph[a[i].u][a[i].i] == 1) cout << "TRUE" << endl;
            else cout << "FALSE" << endl;
        }
        else {
            check = false;
            for (int j = 0; j < v; j++) {
                 
                if (graph[a[i].u][j] == 1) {
                    check = true;
                    cout << j + 1 << " ";
                }
                
            }
            if (check == false) cout << "NONE";
            cout << endl;
        }
    }
}
 
int main() {
    int v, e, n;
    cin >> v >> e >> n;
    vector<vector<bool>> graph(v,vector<bool>(v));
    for (int i = 0; i < e; i++) {
        int u, j;
        cin >> u >> j;
        graph[u-1][j-1] = 1;
    }
    //for (int i = 0; i < v; i++) {
    //    for (int j = 0; j < v; j++)
    //        cout << graph[i][j] << " ";
    //    cout << endl;
    //}
    handling *a = new handling[n];
    for (int i = 0; i < n; i++) {
        nhap(a[i]);
    }
    output(graph, v, n, a);
//    system("pause");
    return 0;
}