#include<iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
using namespace std;
 
bool isStore(vector<int> tVec, int x) {
    for (int i = 0; i < tVec.size(); i++) {
        if (x == tVec[i]) return true;
    }
    return false;
}
 
 
 
int minColor(vector<int> tVec, vector<int> vec) {
    int min = 0;
    sort(vec.begin(), vec.end());
    sort(tVec.begin() ,tVec.end());
    for (int i = 0; i <vec.size(); i++) {
        if (!isStore(tVec, vec[i])) return vec[i];
    }
    for (int i = 0; i <16777216; i++) {
        if (!isStore(tVec, i)) return i;
    }
    return min; 
}
 
int main() {
    int v, e, n;
    cin >> v >> e >> n;
    map<string, int> mymap;
    string *data = new string[v];
    vector<int> vec;
    int *color = new int[v];
    for (int i = 0; i < v; i++) {
        cin >> data[i];
        mymap[data[i]] = i;
    }
    vector<vector<bool>> graph(v, vector<bool>(v));
    for (int i = 0; i < e; i++) {
        string u, j;
        cin >> u >> j;
        graph[mymap[u]][mymap[j]] = 1;
    }
    /*for (int i = 0; i < e; i++) {
        for (int j = 0; j < e; j++)
            cout << graph[i][j]<<" ";
        cout << endl;
    }*/
 
    for (int i = 0; i < v; i++) {
        cin >> color[i];
        if (color[i] != -1) vec.push_back(color[i]);
    }
    for (int i = 0; i < n; i++) {
        string temp;
        cin >> temp;
        if (color[mymap[temp]] != -1) {
            bool b = true;
            for (int j = 0; j < v; j++) {
                if (graph[mymap[temp]][j] == 1 || graph[j][mymap[temp]] == 1) {
                    if (color[mymap[temp]] == color[j])
                        b = false;
                }
            }
            if (b) 
                cout << "TRUE\n";
            else {
                vector<int> tVec;
                for (int j = 0; j < v; j++) {
                    if ((graph[mymap[temp]][j] == 1 || graph[j][mymap[temp]] == 1)
                        && color[j] != -1) {
                        //cout << j << ":" << color[j] << endl;
                        tVec.push_back(color[j]);
                    }
 
                }
                //color[mymap[temp]] = minColor(tVec,vec);
                cout << minColor(tVec, vec) << endl;
            }
        }
        else {
            vector<int> tVec;
            for (int j = 0; j < v; j++) {
                if ((graph[mymap[temp]][j] == 1 || graph[j][mymap[temp]] == 1)
                    && color[j]!=-1) {
                    //cout << j << ":" << color[j] << endl;
                    tVec.push_back(color[j]);
                }
 
            }
        //    color[mymap[temp]] = minColor(tVec,vec);
            cout << minColor(tVec, vec) << endl;
        }
    }
    
    delete[] data;
    delete[] color;
    system("pause");
    return 0;
}