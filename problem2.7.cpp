#include <iostream>
#include <vector>
#include <algorithm>
 
using namespace std;
 
 
int sum(vector<int> mc, int **dt, int *dt1, int n) {
 
    for (int i = 0; i < n; i++) {
        if (dt[dt1[i]][i] < 0)
            return -1;
        mc[dt1[i]] += dt[dt1[i]][i];
    }
    int max = mc[0];
    for (int i = 0; i < mc.size(); i++) {
        if (mc[i] > max)
            max = mc[i];
    }
    return max;
}
 
int min = 999999999;
void bruteForce(vector<int> mc, int **dt, int* dt1, int *rs, int m, int n, int len) {
    if (len == n) {
    
        if (sum(mc, dt, dt1, n) <= ::min && sum(mc, dt, dt1, n) > 0) {
            ::min = sum(mc, dt, dt1, n);
            copy(dt1, dt1 + n, rs);
        }
        return;
    }
    for (int i = 0; i < m; i++) {
        dt1[len] = i;
        bruteForce(mc, dt, dt1, rs, m, n, len + 1);
    }
}
 
int main() {
    int n, m;
    cin >> n >> m;
    vector<int> machine;
    for (int i = 0; i < m; i++) {
        machine.push_back(0);
    }
 
    int **data = new int*[m];
    for (int i = 0; i < m; i++)
        data[i] = new int[n];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            cin >> data[i][j];
        }
    }
 
    int * rs = new int[n];
    int * dt1 = new int[n];
 
    
    bruteForce(machine, data, dt1, rs, m, n, 0);
    for (int i = 0; i < n; i++)
        cout << rs[i] << " ";
    cout << endl;
    system("pause");
    return 0;
}