#include <iostream>
#include <vector>
using namespace std;
 
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL); cout.tie(NULL);
    int v, n;
    cin >> v>> n;
    bool **graph = new bool *[v];
    for (int i = 0; i < v; i++)
        graph[i] = new bool[v];
 
    for (short int i = 0; i < v; i++) {
        for (short int j = 0; j < v; j++) {
            cin >> graph[i][j];
        }
    }
 
    for (int i = 0; i < n; i++) {
        int num, u, x;
        cin >> num;
        if (num == 1) {
            cin >> u >> x;
            u--;
            x--;
            if (graph[u][x] == 1) cout << "TRUE" << endl;
            else cout << "FALSE" << endl;
        }
        else {
            cin >> u;
            u--;
            for (short int j = 0; j < v; j++)    
                if (graph[u][j] == 1) {
                    cout << j + 1 << " ";
                }
            cout << endl;
        }
    }
    for (short int i = 0; i < v; i++)
        delete graph[i];
    delete graph;
    return 0;
}