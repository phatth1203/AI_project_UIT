#include <iostream>
#include <vector>
#include <algorithm>
#define infi = 999
using namespace std;
 
struct job {
    int mc, tm, tt;
};
 
int iMin(vector<int> b) {
    int min = b[0];
    int imin = 0;
    for (int i = 0; i < b.size(); i++) {
        if (b[i] <= min) {
            min = b[i];
            imin = i;
        }
    }
    return imin;
}
 
bool cmpBytm( job a, job b) {
    return a.tm < b.tm;
}
 
int sum(vector<int> mc,int *dt,int *dt1,int n) {
 
    for (int i = 0; i < n; i++) {
        mc[dt1[i]] += dt[i];
    }
    int max = mc[0];
    for (int i = 0; i < mc.size(); i++) {
        if (mc[i] > max)
            max = mc[i];
    }
    return max;
}
 
int min = 999999999;
void bruteForce(vector<int> mc,int *dt,int* dt1,int *rs,int m,int n,int len) {
    if (len == n) {
        /*for (int i = 0; i < n; i++)
            cout << dt1[i];
        cout << endl;*/
        if (sum(mc, dt, dt1, n) < ::min) {
            ::min = sum(mc, dt, dt1, n);
            copy(dt1,dt1+n,rs);
        }
        return;
    }
    for (int i = 0; i < m; i++) {
        dt1[len] = i;
        bruteForce(mc, dt,dt1,rs ,m, n, len + 1);
    }
}
 
int main() {
    int n, m;
    cin >> n >> m;
    vector<int> machine;
    int *data = new int[n];
    for (int i = 0; i < m; i++) {
        machine.push_back(0);
    }
    for (int i = 0; i < n; i++)
        cin >> data[i];
    int * rs = new int[n];
    int * dt1 = new int[n];
 
    /*rs[0] = 1;
    rs[1] = 2;
    rs[2] = 1;
    rs[3] = 1;
    rs[4] = 0;
    rs[5] = 2;
    rs[6] = 1;
    rs[7] = 0;
    cout << sum(machine, data, rs, n);*/
    bruteForce(machine, data,dt1,rs, m, n, 0);
    for (int i = 0; i < n; i++)
        cout << rs[i] << " ";
    cout << endl;
    system("pause");
    return 0;
}