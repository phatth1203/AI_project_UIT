    #include <iostream>
    #include <vector>
    using namespace std;
 
    int max(vector<int> data, int m) {
        int max = 0;
        for (int i = 0; i < m; i++) {
            if (data[i] > max)
                max = data[i];
        }
        return max;
    }
 
    int main() {
        int n, m;
        cin >> n >> m;
        vector<int> time;
        vector<int> machine;
        for (int i = 0; i < m; i++)
            machine.push_back(0);
        for (int i = 0; i < n; i++){
            int temp; cin >> temp;
            time.push_back(temp);
        }
        for (int i = 0; i < n; i++) {
            int num; cin >> num;
            machine[num] += time[i];
        }
        cout << max(machine, m);
        system("pause");
        return 0;
    }