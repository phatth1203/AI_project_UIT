#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
 
struct Data {
    int time,i; // thời gian và thứ tự.
};
 
bool compare(Data lhs, Data rhs) {
    return lhs.time < rhs.time; 
}
 
void Dsort(Data *dt, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (dt[i].time > dt[j].time)
                std::swap(dt[i], dt[j]);
        }
    }
}
 
int main() {
    int n; cin >> n;
    Data *dt = new Data[n];
    for (int i = 0; i < n; i++) {
        cin >> dt[i].time;
        dt[i].i = i;
    }
    //Dsort(dt, n);
    //std::sort(dt,dt+n, compare);
    cout << endl;
    for (int i = 0; i < n; i++) {
        int min = 10000;
        int ii = 10000;
        int ix = 10000;
        for (int i = 0; i < n; i++) {
            if (dt[i].time < min) {
                min = dt[i].time;
                ii = i;
            }
        }
        dt[ii].time = 10000;
        cout << ii <<" ";
    }
    delete [] dt;
    system("pause");
    return 0;
}