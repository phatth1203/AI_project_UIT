#include<iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;
 
int convert(map<string,int> mymap, string _s) {
    return mymap[_s];
}
 
int main() {
    int v, e, n;
    cin >> v >> e >> n;
    map<string, int> mymap;
    string *data = new string[v];
    for (int i = 0; i < v; i++) {
        cin >> data[i];
        mymap[data[i]] = i;
    }
    vector<vector<bool>> graph(v, vector<bool>(v));
    for (int i = 0; i < e; i++) {
        string u, j;
        cin >> u >> j;
        graph[mymap[u]][mymap[j]] = 1;
    }
    bool check;
    int number;
    string u;
    for (int i = 0; i < n; i++) {
        cin >> number;
        cin >> u;
        int rr = mymap[u];
        if (number == 1) {
            string j;
            cin >> j;
            int cc = mymap[j];
            if (graph[rr][cc] == 1) cout << "TRUE" << endl;
            else cout << "FALSE" << endl;
        }
        else {
            check = false;
            for (int j = 0; j < v; j++) {
 
                if (graph[rr][j] == 1) {
                    check = true;
                    cout << data[j] << " ";
                }
 
            }
            if (check == false) cout << "NONE";
            cout << endl;
        }
    }
    //    system("pause");
    return 0;
}